# "*SoSe Testverbesserung - CRUD with Vaadin*"

## Aufgabenstellung

Passen Sie das bereitgestellte Beispiel ([gs-crud-with-vaadin](https://github.com/spring-guides/gs-crud-with-vaadin.git)) soweit an, dass die geforderten Funktionen erfüllt werden.

- Erstellen Sie aus dem bereitgestellten astah-Klassendiagramm „letzteSchulwoche“ ein Package „model“ mit allen Klassen. Wie gehen Sie dabei vor?
- Bereiten Sie die Klasse „Veranstaltung“ aus dem erstellten package „model“ zur Verwaltung und Persistierung vor. Welche Annotation(en) ist/sind dabei wichtig? Erläutern Sie diese! Welche zusätzlichen Methoden müssen Sie generieren/erstellen?
- Persistieren Sie mindestens fünf Beispiel-Objekte der erstellten Klasse. Welche Elemente des Frameworks kommen dabei zum Einsatz? Wie stellen Sie sicher, dass die Daten nach einem Neustart des Servers nicht verloren gehen?
- Ermöglichen Sie eine Datenmanipulation (CRUD) auf die Klasse „Veranstaltung“. Welche Elemente der grafischen Oberfläche müssen dabei angepasst werden und wie?

## Implementierung

#### UML-Diagramm zu Java-Code umwandeln:

`letzteSchulwoche` per Astah öffnent, `Tools - Java - Export Java`.

#### Persistieren der Daten:

Bei allen Klassen, die persistiert werden sollen, müssen Setter- und Getter-Methoden erstellt werden.

All diese Klassen besitzen die `@Entity` Annotation bzw. `@Embeddable`. Embeddable bedeutet, dass eine Klasse innerhalb einer anderen Klasse persistiert werden, also das sie eine einzige Tabelle bilden.

Allgemein sind die Annotations `@Id` und `@GeneratedValue` wichtig. `@Id` wird bei Attributen eingefügt, die den Primary Key in der Tabelle bilden sollen.  `@GeneratedValue` besagt, dass die Datenbank diesen Wert automatisch setzen soll.

Bei der Klasse Warteliste musste ich die `@Id`-Annotation entfernen, da diese bei Embeddables ungültig ist.

Damit eine Datenbank verwendet werden soll, muss in der `application.properties` unter `src/main/resources` die Datenbank konfiguriert werden.

Für H2 sieht diese so aus:

```properties
# Datasource
spring.datasource.url=jdbc:h2:file:./data/user_db
spring.datasource.username=admin
spring.datasource.password=userdata
spring.datasource.driver-class-name=org.h2.Driver
# Use "create" or "create-drop" when you wish to recreate database on restart;
# use "update" or "validate" when data is to be kept.
spring.jpa.hibernate.ddl-auto=create-drop
```

Wenn man die Daten dauerhaft persistieren möchte, ohne das jedes mal die Datenbank neu erstellt wird, muss Hibernate `create` eingestellt werden.

Außerdem müssen im Quellcode etwaige Methoden wie `repository.deleteAll()` entfernt werden.

Meine fünf Datensatz Objekte werden in der Klasse `Application` in der Methode `loadData` persistiert:

```java
repository.save(new Veranstaltung("Test", "Ein erster Test", "Testort",
					2.20, false));
			repository.save(new Veranstaltung("Test2", "Ein erster Test", "Testort",
					2.30, false));
			repository.save(new Veranstaltung("Test3", "Ein erster Test", "Testort",
					2.60, false));
			repository.save(new Veranstaltung("Baum", "Weil Baum", "Baumhausen",
					6.60, true));
			repository.save(new Veranstaltung("Konzert", "Sehr toll", "Oberstinkenbrunn",
					6.60, true));
```

#### Vaadin UI

Für das Vaadin-Interface musste die Klasse `MainView` an die Attribute der Klasse Veranstaltung angepasst werden. Außerdem musste alles was im Zusammenhang mit der Klasse Consumer zu tun hatte, auf Veranstaltung angepasst werden. 

Bei der Klasse `VeranstaltungEditor` mussten neue Textfelder für die Attribute erstellt werden und eine Checkbox für das Boolean-Attribut. 

```java
TextField kurzBeschreibung = new TextField("Kurzbeschreibung");
TextField beschreibung = new TextField("Beschreibung");
TextField ort = new TextField("Ort");
TextField kosten = new TextField("Kosten");
Checkbox optRefundierung = new Checkbox("Refundierung");
```

Diese müssen dann per `add()` Methode in das UI-Element eingesetzt werden.

Danach werden die Textfelder bzw. die eine Checkbox auf die Attribute der Klasse Veranstaltung angepasst werden. Dazu verwendet man einen *binder*.

```java
	binder.forField(kurzBeschreibung)
				.asRequired()
				.withValidator(new StringLengthValidator(
						"Beschreibung muss mindestens ein Zeichen enthalten. Maximal 15 sind erlaubt.", 1, 15))
				.bind(Veranstaltung::getKurzBeschreibung, Veranstaltung::setKurzBeschreibung);

		binder.forField(beschreibung)
				.asRequired()
				.bind(Veranstaltung::getBeschreibung, Veranstaltung::setBeschreibung);

		binder.forField(kosten)
				.withConverter(new StringToDoubleConverter("Please enter a number"))
				.bind(Veranstaltung::getKosten, Veranstaltung::setKosten);

		binder.forField(ort)
				.asRequired()
				.bind(Veranstaltung::getOrt, Veranstaltung::setOrt);

		binder.forField(optRefundierung)
				.bind(Veranstaltung::isOptRefundierung, Veranstaltung::setOptRefundierung);

```

## Quellen

* [CRUD with Vaadin - Example Code](https://github.com/spring-guides/gs-crud-with-vaadin)
* [Embeddable funktionstüchtig machen](https://stackoverflow.com/questions/39040315/hibernate-embeddables-component-property-not-found)
* [Vaadin - Textfield Input auf Double casten](https://vaadin.com/docs/v13/flow/binding-data/tutorial-flow-components-binder-validation.html)
* [Vaadin Checkbox](https://vaadin.com/components/vaadin-checkbox/java-examples)