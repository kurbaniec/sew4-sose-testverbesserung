package hello.model;

import java.io.Serializable;

/**
 * Grundlegende Definition der Benutzer. Diese werden \u00FCber die Anbindung an das eDirectory importiert und auch zu bestimmten Zeiten aktualisiert.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
@javax.persistence.MappedSuperclass @javax.persistence.Inheritance(strategy = javax.persistence.InheritanceType.TABLE_PER_CLASS)
public class Benutzer implements Serializable {

	@javax.persistence.Id     @javax.persistence.GeneratedValue
	private Integer benutzerId;

	private String vorname;

	private String nachname;

	@javax.persistence.Column(unique=true)
	private String benutzername;

	@javax.persistence.Column(unique=true)
	private String email;

	public Benutzer() {

	}

	public Integer getBenutzerId() {
		return benutzerId;
	}

	public void setBenutzerId(Integer benutzerId) {
		this.benutzerId = benutzerId;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getBenutzername() {
		return benutzername;
	}

	public void setBenutzername(String benutzername) {
		this.benutzername = benutzername;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
