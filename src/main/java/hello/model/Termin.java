package hello.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Termine sind Teile von Veranstaltungen, die Zusatzinformationen beinhalten und ohne einer Veranstaltung nicht erstellt werden k\u00F6nnen.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 * 
 * @javax.persistence.OneToMany(mappedBy = "termin")
 * private Collection<Teilnahme> teilnahme;
 */
@javax.persistence.Entity
public class Termin implements Serializable {

	@javax.persistence.Id     @javax.persistence.GeneratedValue
	private Integer terminId;

	private String treffpunkt;

	@javax.persistence.Temporal(javax.persistence.TemporalType.TIME)
	private Date startZeit;

	@javax.persistence.Temporal(javax.persistence.TemporalType.TIME)
	private Date ende;

	private boolean findetStatt;

	private Integer maxAnzahlTeilnehmer;

	@javax.persistence.Temporal(javax.persistence.TemporalType.TIME)
	private Date fixeAnmeldungBis;

	@javax.persistence.Embedded
	private Warteliste warteliste;

	/**
	 * @javax.persistence.ManyToMany
	 */
	private Lehrer[] lehrer;

	/**
	 * @javax.persistence.ManyToOne
	 * @javax.persistence.MapsId("schueler_id")
	 */
	private Teilnahme[] teilnahme;

	public Termin() {

	}

	public Integer getTerminId() {
		return terminId;
	}

	public void setTerminId(Integer terminId) {
		this.terminId = terminId;
	}

	public String getTreffpunkt() {
		return treffpunkt;
	}

	public void setTreffpunkt(String treffpunkt) {
		this.treffpunkt = treffpunkt;
	}

	public Date getStartZeit() {
		return startZeit;
	}

	public void setStartZeit(Date startZeit) {
		this.startZeit = startZeit;
	}

	public Date getEnde() {
		return ende;
	}

	public void setEnde(Date ende) {
		this.ende = ende;
	}

	public boolean isFindetStatt() {
		return findetStatt;
	}

	public void setFindetStatt(boolean findetStatt) {
		this.findetStatt = findetStatt;
	}

	public Integer getMaxAnzahlTeilnehmer() {
		return maxAnzahlTeilnehmer;
	}

	public void setMaxAnzahlTeilnehmer(Integer maxAnzahlTeilnehmer) {
		this.maxAnzahlTeilnehmer = maxAnzahlTeilnehmer;
	}

	public Date getFixeAnmeldungBis() {
		return fixeAnmeldungBis;
	}

	public void setFixeAnmeldungBis(Date fixeAnmeldungBis) {
		this.fixeAnmeldungBis = fixeAnmeldungBis;
	}

	public Warteliste getWarteliste() {
		return warteliste;
	}

	public void setWarteliste(Warteliste warteliste) {
		this.warteliste = warteliste;
	}

	public Lehrer[] getLehrer() {
		return lehrer;
	}

	public void setLehrer(Lehrer[] lehrer) {
		this.lehrer = lehrer;
	}

	public Teilnahme[] getTeilnahme() {
		return teilnahme;
	}

	public void setTeilnahme(Teilnahme[] teilnahme) {
		this.teilnahme = teilnahme;
	}
}
