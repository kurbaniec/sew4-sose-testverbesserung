package hello.model;


/**
 * Hier werden die einzelnen Teilnehmerstati als enumeration definiert.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
public enum TeilnahmeStatus {

	;

	public String ANGEMELDET;

	public String WARTEND;

	public String ABWESEND;

	public String ENTSCHULDIGT;

	public String TEILGENOMMEN;

}
