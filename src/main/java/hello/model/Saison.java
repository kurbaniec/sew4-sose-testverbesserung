package hello.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Grundlegende Definition der Saison. Die Saison bestimmt Das zu administrierende Schuljahr wird durch die Saison definiert und hat einen Anmeldestart sowie ein Anmeldeende, wo keine von Sch\u00FClern get\u00E4tigten Teilnahmeanmeldungen mehr akzeptiert werden.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
@javax.persistence.Entity @javax.persistence.NamedQueries({     @javax.persistence.NamedQuery(name = "Saison.findAll", query = "SELECT s FROM Saison s"),     @javax.persistence.NamedQuery(name = "Saison.findBySaisonId", query = "SELECT s FROM Saison s WHERE s.saisonId = :saisonId") })
public class Saison implements Serializable {

	@javax.persistence.Id     @javax.persistence.GeneratedValue
	private Integer saisonId;

	@javax.persistence.Column(unique = true)
	private String schuljahr;

	@javax.persistence.Temporal(javax.persistence.TemporalType.TIME)
	private Date anmeldeStart;

	@javax.persistence.Temporal(javax.persistence.TemporalType.TIME)
	private Date anmeldeEnde;

	/**
	 * @javax.persistence.OneToMany(mappedBy = "saison")
	 */
	private Veranstaltung[] veranstaltung;

	public Saison() {

	}

	public Integer getSaisonId() {
		return saisonId;
	}

	public void setSaisonId(Integer saisonId) {
		this.saisonId = saisonId;
	}

	public String getSchuljahr() {
		return schuljahr;
	}

	public void setSchuljahr(String schuljahr) {
		this.schuljahr = schuljahr;
	}

	public Date getAnmeldeStart() {
		return anmeldeStart;
	}

	public void setAnmeldeStart(Date anmeldeStart) {
		this.anmeldeStart = anmeldeStart;
	}

	public Date getAnmeldeEnde() {
		return anmeldeEnde;
	}

	public void setAnmeldeEnde(Date anmeldeEnde) {
		this.anmeldeEnde = anmeldeEnde;
	}

	public Veranstaltung[] getVeranstaltung() {
		return veranstaltung;
	}

	public void setVeranstaltung(Veranstaltung[] veranstaltung) {
		this.veranstaltung = veranstaltung;
	}
}
