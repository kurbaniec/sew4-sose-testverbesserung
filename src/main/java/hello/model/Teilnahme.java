package hello.model;


import java.io.Serializable;

/**
 * Eine Teilnahme wird durch einen Termin und einen entsprechenden Sch\u00FCler definiert. Dabei soll ein Sch\u00FCler mindestens bei drei Terminen angemeldet werden. Die Teilnahme ist jedoch als Assoziationsklasse zwischen einzelnen Terminen und den Sch\u00FClern implementiert.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
@javax.persistence.Entity
public class Teilnahme implements Serializable {

	@javax.persistence.EmbeddedId
	private TeilnahmeId teilnahmeId;

	private boolean bezahlt;

	@javax.persistence.Enumerated(javax.persistence.EnumType.STRING)
	private TeilnahmeStatus status;

	/**
	 * @javax.persistence.ManyToOne
	 * @javax.persistence.MapsId("termin_id")
	 */
	private Termin termin;

	/**
	 * @javax.persistence.ManyToOne
	 * @javax.persistence.MapsId("schueler_id")
	 */
	private Schueler schueler;

	public Teilnahme() {

	}

	public TeilnahmeId getTeilnahmeId() {
		return teilnahmeId;
	}

	public void setTeilnahmeId(TeilnahmeId teilnahmeId) {
		this.teilnahmeId = teilnahmeId;
	}

	public boolean isBezahlt() {
		return bezahlt;
	}

	public void setBezahlt(boolean bezahlt) {
		this.bezahlt = bezahlt;
	}

	public TeilnahmeStatus getStatus() {
		return status;
	}

	public void setStatus(TeilnahmeStatus status) {
		this.status = status;
	}

	public Termin getTermin() {
		return termin;
	}

	public void setTermin(Termin termin) {
		this.termin = termin;
	}

	public Schueler getSchueler() {
		return schueler;
	}

	public void setSchueler(Schueler schueler) {
		this.schueler = schueler;
	}
}
