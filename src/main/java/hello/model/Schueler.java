package hello.model;


import java.io.Serializable;

/**
 * Grundlegende Definition der Sch\u00FCler. Hier werden nur die Benutzer aus den importierten Sch\u00FClergruppen aufgenommen.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 * 
 * @OneToMany(mappedBy = "schueler")
 * private Collection<Teilnahme> teilnahme;
 */
@javax.persistence.Entity @javax.persistence.NamedQueries({     @javax.persistence.NamedQuery(name = "Schueler.findAll", query = "SELECT s FROM Schueler s"),     @javax.persistence.NamedQuery(name = "Schueler.findByBenutzerId", query = "SELECT s FROM Schueler s WHERE s.benutzerId = :benutzerId"),     @javax.persistence.NamedQuery(name = "Schueler.findByEmail", query = "SELECT s FROM Schueler s WHERE s.email = :email"),     @javax.persistence.NamedQuery(name = "Schueler.findByUsername", query = "SELECT s FROM Schueler s WHERE s.benutzername = :benutzername")})
public class Schueler extends Benutzer implements Serializable {

	private String jahrgang;

	private Double guthaben;

	/**
	 * @javax.persistence.ManyToOne
	 * @javax.persistence.MapsId("termin_id")
	 */
	private Teilnahme[] teilnahme;

	public Schueler() {

	}

	public String getJahrgang() {
		return jahrgang;
	}

	public void setJahrgang(String jahrgang) {
		this.jahrgang = jahrgang;
	}

	public Double getGuthaben() {
		return guthaben;
	}

	public void setGuthaben(Double guthaben) {
		this.guthaben = guthaben;
	}

	public Teilnahme[] getTeilnahme() {
		return teilnahme;
	}

	public void setTeilnahme(Teilnahme[] teilnahme) {
		this.teilnahme = teilnahme;
	}
}
