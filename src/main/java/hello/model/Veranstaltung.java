package hello.model;


import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Eine Veranstaltung ist als Grundinformation zu betrachten. Hier werden die allgemeine Beschreibung, der Ort sowie die Kosten gespeichert. Details zum Treffpunkt und zur Teilnehmeranzahl finden sich in den zugeh\u00F6rigen Terminen.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
@javax.persistence.Entity @javax.persistence.NamedQueries({     @javax.persistence.NamedQuery(name = "Veranstaltung.findAll", query = "SELECT v FROM Veranstaltung v"),     @javax.persistence.NamedQuery(name = "Veranstaltung.findByVeranstaltungId", query = "SELECT v FROM Veranstaltung v WHERE v.veranstaltungId = :veranstaltungId"),     @javax.persistence.NamedQuery(name = "Veranstaltung.findByBeschreibung", query = "SELECT v FROM Veranstaltung v WHERE v.beschreibung = :beschreibung"),     @javax.persistence.NamedQuery(name = "Veranstaltung.findByOrt", query = "SELECT v FROM Veranstaltung v WHERE v.ort = :ort")})
public class Veranstaltung implements Serializable {

	@javax.persistence.Id     @javax.persistence.GeneratedValue
	// from Integer to Long
	private Long veranstaltungId;

	@javax.validation.constraints.Size(min = 1, max = 15)     @javax.persistence.Column(unique = true)
	private String kurzBeschreibung;

	private String beschreibung;

	private String ort;

	private Double kosten;

	private boolean optRefundierung;

	/**
	 * @javax.persistence.OneToMany(mappedBy = "veranstaltung")
	 */
	private Termin[] termin;

	/**
	 * @javax.persistence.ManyToOne
	 */
	private Saison saison;

	public Veranstaltung() {

	}

	public Veranstaltung(@Size(min = 1, max = 15) String kurzBeschreibung, String beschreibung, String ort, Double kosten, boolean optRefundierung) {
		this.kurzBeschreibung = kurzBeschreibung;
		this.beschreibung = beschreibung;
		this.ort = ort;
		this.kosten = kosten;
		this.optRefundierung = optRefundierung;
	}

	public Veranstaltung(@Size(min = 1, max = 15) String kurzBeschreibung, String beschreibung, String ort) {
		this.kurzBeschreibung = kurzBeschreibung;
		this.beschreibung = beschreibung;
		this.ort = ort;
		this.kosten = 0.0;
		this.optRefundierung = false;
	}

	public Long getVeranstaltungId() {
		return veranstaltungId;
	}

	public void setVeranstaltungId(Long veranstaltungId) {
		this.veranstaltungId = veranstaltungId;
	}

	public String getKurzBeschreibung() {
		return kurzBeschreibung;
	}

	public void setKurzBeschreibung(String kurzBeschreibung) {
		this.kurzBeschreibung = kurzBeschreibung;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public Double getKosten() {
		return kosten;
	}

	public void setKosten(Double kosten) {
		this.kosten = kosten;
	}

	public boolean isOptRefundierung() {
		return optRefundierung;
	}

	public void setOptRefundierung(boolean optRefundierung) {
		this.optRefundierung = optRefundierung;
	}

	public Termin[] getTermin() {
		return termin;
	}

	public void setTermin(Termin[] termin) {
		this.termin = termin;
	}

	public Saison getSaison() {
		return saison;
	}

	public void setSaison(Saison saison) {
		this.saison = saison;
	}

	@Override
	public String toString() {
		return "Veranstaltung{" +
				"veranstaltungId=" + veranstaltungId +
				", kurzBeschreibung='" + kurzBeschreibung + '\'' +
				", beschreibung='" + beschreibung + '\'' +
				", ort='" + ort + '\'' +
				", kosten=" + kosten +
				", optRefundierung=" + optRefundierung +
				", termin=" + Arrays.toString(termin) +
				", saison=" + saison +
				'}';
	}
}
