package hello.model;


import java.io.Serializable;

/**
 * Eine Warteliste ist genau einem Termin zugeordnet und wird mit diesem auch wieder entfernt. Eine Warteliste kann nicht f\u00FCr sich alleine bestehen.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
@javax.persistence.Embeddable
public class Warteliste implements Serializable {

	//@javax.persistence.Id
	@javax.persistence.GeneratedValue
	private Integer wartelisteId;

	private Integer maxAnzahlPlaetze;

	public Warteliste() {

	}

	public Integer getWartelisteId() {
		return wartelisteId;
	}

	public void setWartelisteId(Integer wartelisteId) {
		this.wartelisteId = wartelisteId;
	}

	public Integer getMaxAnzahlPlaetze() {
		return maxAnzahlPlaetze;
	}

	public void setMaxAnzahlPlaetze(Integer maxAnzahlPlaetze) {
		this.maxAnzahlPlaetze = maxAnzahlPlaetze;
	}
}
