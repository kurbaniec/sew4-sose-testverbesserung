package hello.model;

import java.io.Serializable;

/**
 * Grundlegende Definition der Lehrer. Hier werden nur die Benutzer aus der importierten Lehrergruppe aufgenommen.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
@javax.persistence.Entity @javax.persistence.NamedQueries({     @javax.persistence.NamedQuery(name = "Lehrer.findAll", query = "SELECT l FROM Lehrer l"),     @javax.persistence.NamedQuery(name = "Lehrer.findByBenutzerId", query = "SELECT l FROM Lehrer l WHERE l.benutzerId = :benutzerId"),     @javax.persistence.NamedQuery(name = "Lehrer.findByEmail", query = "SELECT l FROM Lehrer l WHERE l.email = :email"),     @javax.persistence.NamedQuery(name = "Lehrer.findByUsername", query = "SELECT l FROM Lehrer l WHERE l.benutzername = :benutzername")})
public class Lehrer extends Benutzer implements Serializable {

	private String jahrgangsvorstand;

	/**
	 * @javax.persistence.ManyToMany(mappedBy = "lehrer")
	 */
	private Termin[] termin;

	public Lehrer() {

	}

	public String getJahrgangsvorstand() {
		return jahrgangsvorstand;
	}

	public void setJahrgangsvorstand(String jahrgangsvorstand) {
		this.jahrgangsvorstand = jahrgangsvorstand;
	}

	public Termin[] getTermin() {
		return termin;
	}

	public void setTermin(Termin[] termin) {
		this.termin = termin;
	}
}
