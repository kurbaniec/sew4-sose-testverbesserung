package hello.model;


import java.io.Serializable;

/**
 * Zusammengesetzter Schl\u00FCssel von Sch\u00FCler_ID und Termin_ID.
 * 
 * @author Michael Borko <michael.borko@tgm.ac.at>
 */
@javax.persistence.Embeddable
public class TeilnahmeId implements Serializable {

	private Integer schuelerId;

	private Integer terminId;

	public Integer getSchuelerId() {
		return schuelerId;
	}

	public void setSchuelerId(Integer schuelerId) {
		this.schuelerId = schuelerId;
	}

	public Integer getTerminId() {
		return terminId;
	}

	public void setTerminId(Integer terminId) {
		this.terminId = terminId;
	}
}
