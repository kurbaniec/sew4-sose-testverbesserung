package hello;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import hello.model.Veranstaltung;
import org.springframework.util.StringUtils;

@Route
public class MainView extends VerticalLayout {

	private final VeranstaltungRepository repo;

	private final VeranstaltungEditor editor;

	final Grid<Veranstaltung> grid;

	final TextField filter;

	private final Button addNewBtn;

	/**
		@javax.persistence.Id     @javax.persistence.GeneratedValue
		private Integer veranstaltungId;

		@javax.validation.constraints.Size(min = 1, max = 15)     @javax.persistence.Column(unique = true)
		private String kurzBeschreibung;

		private String beschreibung;

		private String ort;

		private Double kosten;

		private boolean optRefundierung;
	*/


	public MainView(VeranstaltungRepository repo, VeranstaltungEditor editor) {
		this.repo = repo;
		this.editor = editor;
		this.grid = new Grid<>(Veranstaltung.class);
		this.filter = new TextField();
		this.addNewBtn = new Button("New Veranstaltung", VaadinIcon.PLUS.create());

		// build layout
		HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
		add(actions, grid, editor);

		grid.setHeight("300px");
		// from id -> veranstaltungId
		// added new columns
		grid.setColumns("veranstaltungId", "kurzBeschreibung", "beschreibung", "ort", "kosten", "optRefundierung");
		grid.getColumnByKey("veranstaltungId").setWidth("50px").setFlexGrow(0);

		filter.setPlaceholder("Filter by Kurz Beschreibung");

		// Hook logic to components

		// Replace listing with filtered content when user changes filter
		filter.setValueChangeMode(ValueChangeMode.EAGER);
		filter.addValueChangeListener(e -> listCustomers(e.getValue()));

		// Connect selected Customer to editor or hide if none is selected
		grid.asSingleSelect().addValueChangeListener(e -> {
			editor.editVeranstaltung(e.getValue());
		});

		// Instantiate and edit new Customer the new button is clicked
		addNewBtn.addClickListener(e -> editor.editVeranstaltung(new Veranstaltung("", "", "")));

		// Listen changes made by the editor, refresh data from backend
		editor.setChangeHandler(() -> {
			editor.setVisible(false);
			listCustomers(filter.getValue());
		});

		// Initialize listing
		listCustomers(null);
	}

	// tag::listCustomers[]
	void listCustomers(String filterText) {
		if (StringUtils.isEmpty(filterText)) {
			grid.setItems(repo.findAll());
		}
		else {
			grid.setItems(repo.findByKurzBeschreibungStartsWithIgnoreCase(filterText));
		}
	}
	// end::listCustomers[]

}
