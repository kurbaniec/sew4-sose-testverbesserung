package hello;

import hello.model.Veranstaltung;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VeranstaltungRepository extends JpaRepository<Veranstaltung, Long> {

	List<Veranstaltung> findByKurzBeschreibungStartsWithIgnoreCase(String lastName);
}
