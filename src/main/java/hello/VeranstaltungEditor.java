package hello;

import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import hello.model.Veranstaltung;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * A simple example to introduce building forms. As your real application is probably much
 * more complicated than this example, you could re-use this form in multiple places. This
 * example component is only used in MainView.
 * <p>
 * In a real world application you'll most likely using a common super class for all your
 * forms - less code, better UX.
 */
@SpringComponent
@UIScope
public class VeranstaltungEditor extends VerticalLayout implements KeyNotifier {

	private final VeranstaltungRepository repository;

	/**
	 * The currently edited veranstaltung
	 */
	private Veranstaltung veranstaltung;

	/*
	private String kurzBeschreibung;

	private String beschreibung;

	private String ort;

	private Double kosten;

	private boolean optRefundierung;
	 */

	/* Fields to edit properties in Veranstaltung entity */
	TextField kurzBeschreibung = new TextField("Kurzbeschreibung");
	TextField beschreibung = new TextField("Beschreibung");
	TextField ort = new TextField("Ort");
	TextField kosten = new TextField("Kosten");
	Checkbox optRefundierung = new Checkbox("Refundierung");

	/* Action buttons */
	Button save = new Button("Save", VaadinIcon.CHECK.create());
	Button cancel = new Button("Cancel");
	Button delete = new Button("Delete", VaadinIcon.TRASH.create());
	HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

	Binder<Veranstaltung> binder = new Binder<>(Veranstaltung.class);
	private ChangeHandler changeHandler;

	@Autowired
	public VeranstaltungEditor(VeranstaltungRepository repository) {
		this.repository = repository;

		add(kurzBeschreibung, beschreibung, ort, kosten, optRefundierung, actions);

		binder.forField(kurzBeschreibung)
				.asRequired()
				.withValidator(new StringLengthValidator(
						"Beschreibung muss mindestens ein Zeichen enthalten. Maximal 15 sind erlaubt.", 1, 15))
				.bind(Veranstaltung::getKurzBeschreibung, Veranstaltung::setKurzBeschreibung);

		binder.forField(beschreibung)
				.asRequired()
				.bind(Veranstaltung::getBeschreibung, Veranstaltung::setBeschreibung);

		binder.forField(kosten)
				.withConverter(new StringToDoubleConverter("Please enter a number"))
				.bind(Veranstaltung::getKosten, Veranstaltung::setKosten);

		binder.forField(ort)
				.asRequired()
				.bind(Veranstaltung::getOrt, Veranstaltung::setOrt);

		binder.forField(optRefundierung)
				.bind(Veranstaltung::isOptRefundierung, Veranstaltung::setOptRefundierung);


		// bind using naming convention
		binder.bindInstanceFields(this);

		// Configure and style components
		setSpacing(true);

		save.getElement().getThemeList().add("primary");
		delete.getElement().getThemeList().add("error");

		addKeyPressListener(Key.ENTER, e -> save());

		// wire action buttons to save, delete and reset
		save.addClickListener(e -> save());
		delete.addClickListener(e -> delete());
		cancel.addClickListener(e -> editVeranstaltung(veranstaltung));
		setVisible(false);
	}

	void delete() {
		repository.delete(veranstaltung);
		changeHandler.onChange();
	}

	void save() {
		if (binder.writeBeanIfValid(veranstaltung)) {
			repository.save(veranstaltung);
			changeHandler.onChange();
		}
	}

	public interface ChangeHandler {
		void onChange();
	}

	public final void editVeranstaltung(Veranstaltung c) {
		if (c == null) {
			setVisible(false);
			return;
		}
		final boolean persisted = c.getVeranstaltungId() != null;
		if (persisted) {
			// Find fresh entity for editing
			veranstaltung = repository.findById(c.getVeranstaltungId()).get();
		}
		else {
			veranstaltung = c;
		}
		cancel.setVisible(persisted);

		// Bind veranstaltung properties to similarly named fields
		// Could also use annotation or "manual binding" or programmatically
		// moving values from fields to entities before saving
		binder.setBean(veranstaltung);

		setVisible(true);

		// Focus first name initially
		kurzBeschreibung.focus();
	}

	public void setChangeHandler(ChangeHandler h) {
		// ChangeHandler is notified when either save or delete
		// is clicked
		changeHandler = h;
	}

}
