package hello;

import hello.model.Veranstaltung;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class);
	}


	@Bean
	public CommandLineRunner loadData(VeranstaltungRepository repository) {
		return (args) -> {
			// save a couple of customers
			repository.save(new Veranstaltung("Test", "Ein erster Test", "Testort",
					2.20, false));
			repository.save(new Veranstaltung("Test2", "Ein erster Test", "Testort",
					2.30, false));
			repository.save(new Veranstaltung("Test3", "Ein erster Test", "Testort",
					2.60, false));
			repository.save(new Veranstaltung("Baum", "Weil Baum", "Baumhausen",
					6.60, true));
			repository.save(new Veranstaltung("Konzert", "Sehr toll", "Oberstinkenbrunn",
					6.60, true));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Veranstaltung v : repository.findAll()) {
				log.info(v.toString());
			}
			log.info("");

		};
	}

}
